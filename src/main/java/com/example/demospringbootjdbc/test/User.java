package com.example.demospringbootjdbc.test;

import lombok.Data;

@Data
public class User {

    private Integer id;
    private String username;
    private String password;

}
