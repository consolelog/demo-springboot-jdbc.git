package com.example.demospringbootjdbc.test;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangbin
 */
@RestController
public class TestController {
    @Resource
    JdbcTemplate jdbcTemplate;

    @GetMapping("/test")
    public List<User> list() {
        return jdbcTemplate.query("select * from t_user", (rs, num) -> {
            User user = new User();
            user.setId(rs.getInt("id"));
            user.setUsername(rs.getNString("username"));
            user.setPassword(rs.getNString("password"));
            return user;
        });
    }
}
