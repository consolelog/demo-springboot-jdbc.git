drop database if exists `test-jdbc`;
create database `test-jdbc`;
use `test-jdbc`;
create table t_user
(
    id       int auto_increment
        primary key,
    username varchar(32) null,
    password varchar(64) null
)
    comment '用户表';

